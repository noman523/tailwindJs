let prompInput = prompt("Type here");

let input = document.getElementById("inputEl");
let inputMeter = document.getElementById("inputMeter");
let inputfeet = document.getElementById("inputfeet");
let inputLitre = document.getElementById("inputLitre");
let inputGallon = document.getElementById("inputGallon");

input.innerHTML = prompInput;
inputMeter.innerHTML = prompInput;
inputfeet.innerHTML = prompInput;
inputLitre.innerHTML = prompInput;
inputGallon.innerHTML = prompInput;

// let input = document.getElementById("inputEl").innerHTML;
let outputFeet = document.getElementById("outputFeet");
let outputMeter = document.getElementById("outputMeter");
let outputGallon = document.getElementById("outputGallon");
let outputLitre = document.getElementById("outputLitre");

let count = 0;

function meterToFeet() {
  count = parseInt(prompInput) * 3.28084;
  outputFeet.innerHTML = count;
}

function feetToMeter() {
  count = parseInt(prompInput) * 0.3048;
  outputMeter.innerHTML = count;
}

function feetToGallon() {
  count = parseInt(prompInput) * 0.2641720524;
  outputGallon.innerHTML = count;
}

function feetToLitre() {
  count = parseInt(prompInput) * 4.54609;
  outputLitre.innerHTML = count;
}

meterToFeet();
feetToMeter();
feetToGallon();
feetToLitre();

console.log(inputfeet, outputFeet, prompInput);
